/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
//import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 *
 * @author CBEIH
 */
public final class Diretorio {

    private String home;
    private String diretorioPrincipal;
    private final String diretorioDB;
    private final String diretorioFiles;
    private final String diretorioResultados;
    private String path;

    /**
     *
     */
    public Diretorio() {
        this.home = findHome();
        this.diretorioPrincipal = "/GLf";
        this.path = this.home + this.diretorioPrincipal;
        this.diretorioDB = this.path + "/DB";
        this.diretorioFiles = this.path + "/Files";
        this.diretorioResultados = this.path + "/Resultados";
        constroiDiretorioAplicacao();
    }

    /**
     *
     * @return
     */
    public String getPath() {
        return path;
    }

    /**
     *
     * @param path
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     *
     * @return
     */
    public String getHome() {
        return home;
    }

    /**
     *
     * @param home
     */
    public void setHome(String home) {
        this.home = home;
    }

    /**
     *
     * @return
     */
    public String findHome() {
        String diretorioHome = System.getProperty("user.home");
        setHome(diretorioHome);
        return diretorioHome;
    }

    /**
     *
     * @param diretorio
     */
    public void setDiretorioPrincipal(String diretorio) {
        this.diretorioPrincipal = diretorio;
    }

    /**
     *
     */
    public void constroiDiretorioAplicacao() {
        criarDiretorio(this.path);
        criarDiretorio(this.diretorioDB);
        criarDiretorio(this.diretorioFiles);
        criarDiretorio(this.diretorioResultados);

    }

    /**
     *
     * @param fileName
     * @return
     */
    public boolean checkFileExist(String fileName) {
        String caminho = this.path + "/Files/" + fileName;
        File dir = new File(caminho);
        return dir.exists();
    }

    /**
     *
     * @param fileName
     * @param stringUrl
     * @param pathLocal
     * @return
     */
    public boolean checkFileisCurrent(String fileName, String stringUrl, String pathLocal) {
        File file1, file2 = null;
        long sizeFile1, sizeFile2 = 0;
        String caminho = this.diretorioFiles + "/" + fileName;
        file1 = new File(caminho);
        sizeFile1 = file1.length();

        try {
            URL url = new URL(stringUrl);
            String nomeArquivoLocal = "/" + "_temp_" + fileName;
            try (
                    InputStream is = url.openStream(); ByteArrayOutputStream fos = new ByteArrayOutputStream()) {
                //Le e grava byte a byte. Voce pode (e deve) usar buffers para
                //melhor performance (BufferedReader).                        
                int umByte;
                while ((umByte = is.read()) != -1) {
                    fos.write(umByte);

                }
                //Nao se esqueca de sempre fechar as streams apos seu uso!
                is.close();
            }

            //apos criar o arquivo fisico, retorna referencia para o mesmo
            file2 = new File(pathLocal + nomeArquivoLocal);
            sizeFile2 = file2.length();
        } catch (Exception e) {
        }
        if (sizeFile1 == sizeFile2) {
            if (file2 != null) {
                file2.delete();
            }
            return true;
        } else {
            if (file2 != null) {
                file2.renameTo(file1);
            }
            return false;
        }

    }

    /**
     *
     * @param dbName
     * @throws IOException
     */
    public void downloadDBFromServer(String dbName) throws IOException {
        if (!checkDBExist(dbName)) {
            Diretorio.gravaArquivoDeURL("http://www.cbeih.org/GLf/Files/glf.db", this.diretorioDB + "/glf.db");
        }
    }

    /**
     *
     * @param fileName
     * @param stringUrl
     * @param pathLocal
     * @return
     */
    public boolean checkDBisCurrent(String fileName, String stringUrl, String pathLocal) {
        File file1, file2 = null;
        long sizeFile1, sizeFile2 = 0;
        String caminho = this.diretorioDB + "/" + fileName;
        file1 = new File(caminho);
        sizeFile1 = file1.length();

        try {
            URL url = new URL(stringUrl);
            String nomeArquivoLocal = "/glf_temp.db";
            FileOutputStream fos;
            try (
                    InputStream is = url.openStream()) {
                fos = new FileOutputStream(pathLocal + nomeArquivoLocal);
                //Le e grava byte a byte. Voce pode (e deve) usar buffers para
                //melhor performance (BufferedReader).                        
                int umByte;
                while ((umByte = is.read()) != -1) {
                    fos.write(umByte);
                }
                //Nao se esqueca de sempre fechar as streams apos seu uso!
            }
            fos.close();
            //apos criar o arquivo fisico, retorna referencia para o mesmo
            file2 = new File(pathLocal + nomeArquivoLocal);
            sizeFile2 = file2.length();
        } catch (Exception e) {
        }
        if (sizeFile1 == sizeFile2) {
            if (file2 != null) {
                file2.delete();
            }
            return true;
        } else {
            if (file2 != null) {
                file2.renameTo(file1);
            }
            return false;
        }

    }

    /**
     *
     * @param fileName
     * @return
     */
    public boolean checkDBExist(String fileName) {
        String caminho = this.diretorioDB + "/" + fileName;
        File dir = new File(caminho);
        return dir.exists();
    }

    /**
     *
     * @param diretorio
     * @return
     */
    public static boolean criarDiretorio(String diretorio) {
        try {
            File dir;
            dir = new File(diretorio);
            if (!dir.exists()) {
                boolean mkdir = dir.mkdir();
            }
            return true;
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Erro ao criar o diretorio");
            return false;
        }
    }

    /*
    public static String getDiretorioFromUser() {
        try {
            JFileChooser fc = new JFileChooser();

            // restringe a amostra a diretorios apenas
            fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

            fc.setDialogTitle("Selecione uma pasta");

            int res = fc.showOpenDialog(null);

            File diretorio;

            String diretorioStr = null;
            if (res == JFileChooser.APPROVE_OPTION) {
                diretorio = fc.getSelectedFile();
                diretorioStr = diretorio.getAbsolutePath();
            }
            return diretorioStr;
        } catch (Exception ex) {
            return null;
        }

    }*/
    /**
     *
     * @param stringUrl
     * @param pathLocal
     * @return
     */
    public static File gravaArquivoDeURL(String stringUrl, String pathLocal) {
        try {

            //Encapsula a URL num objeto java.net.URL
            URL url = new URL(stringUrl);
            //Queremos o arquivo local com o mesmo nome descrito na URL
            //Lembrando que o URL.getPath() ira retornar a estrutura 
            //completa de diretorios e voce deve tratar esta String
            //caso nao deseje preservar esta estrutura no seu disco local.
            FileOutputStream fos;
            //... e de escrita.
            try ( //Cria streams de leitura (este metodo ja faz a conexao)...
                    InputStream is = url.openStream()) {
                //... e de escrita.
                fos = new FileOutputStream(pathLocal);
                //Le e grava byte a byte. Voce pode (e deve) usar buffers para
                //melhor performance (BufferedReader).                        
                int umByte;
                while ((umByte = is.read()) != -1) {
                    fos.write(umByte);
                }
                //Nao se esqueca de sempre fechar as streams apos seu uso!
            }
            fos.close();
            //apos criar o arquivo fisico, retorna referencia para o mesmo
            return new File(pathLocal);
        } catch (Exception e) {
        }
        return null;
    }

    /**
     *
     * @param quantidade
     * @return
     */
    public List<Resultado> carregarResultados(int quantidade) {
        int max;
        List lista;
        lista = new ArrayList();
        File imgList[];
        DateFormat formatData = new SimpleDateFormat("dd/MM/yyyy");
        imgList = new File(this.diretorioResultados).listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.toLowerCase().endsWith(".png");
            }
        });
        if (quantidade == 0) {
            max = imgList.length;
        } else if (quantidade < imgList.length) {
            max = quantidade;
        } else {
            max = imgList.length;
        }
        for (int i = 0; i < max; i++) {
            File img = imgList[i];
            String nome = img.getName();
            String dt_ateracao = formatData.format(new Date(img.lastModified()));
            String pathImg;
            pathImg = img.getAbsolutePath();
            Resultado resultado = new Resultado(nome, dt_ateracao, pathImg);
            boolean add = lista.add(resultado);
        }
        return lista;
    }

    public boolean verificaNomeSimulacao(List<Resultado> listResultado, String nome) {

        for (Resultado r : listResultado) {
            if (r.getNome().equals(nome)) {
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @return
     */
    public String getDiretorioPrincipal() {
        return diretorioPrincipal;
    }

    /**
     *
     * @return
     */
    public String getDiretorioDB() {
        return diretorioDB;
    }

    /**
     *
     * @return
     */
    public String getDiretorioFiles() {
        return diretorioFiles;
    }

    public String getDiretorioResultados() {
        return diretorioResultados;
    }

    public ArrayList<Matriz> selectMatrizUser() throws IOException {
        int max;
        ArrayList<Matriz> lista;
        lista = new ArrayList<>();
        File matriz[];
        matriz = new File(this.diretorioFiles).listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.toLowerCase().startsWith("_user_");
            }
        });
        max = matriz.length;
        for (int i = 0; i < max; i++) {
            File m = matriz[i];
            String nome = m.getName();
            String pathMatriz = m.getAbsolutePath();
            Matriz mObj = new Matriz(nome, "brasil_4km_altitude_NEW.csv", "brasil_4km_2015_densidade.csv", 273, 47, 25, 0, 1.0, 1.0,
                    0.125, 0.125, 0.95, 0.25, 0.99, 0.5, 15, 100, 0.40, 400, pathMatriz,
                    "http://www.cbeih.org/GLf/Files/brasil_4km_altitude_NEW.csv",
                    "http://www.cbeih.org/GLf/Files/brasil_4km_2015_densidade.csv", m.getName());
            boolean add = lista.add(mObj);
        }
        return lista;
    }

    public boolean deleteFile(String fileName) {
        File file;
        file = new File(fileName);
        boolean result;

        if (file.exists() && file.isFile()) {
            result = file.delete();
            return result;
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return "Diretorio{" + "home=" + home + ", diretorioPrincipal=" + diretorioPrincipal + ", diretorioDB=" + diretorioDB + ", diretorioFiles=" + diretorioFiles + ", diretorioResultados=" + diretorioResultados + ", path=" + path + '}';
    }

}
