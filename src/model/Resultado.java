/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author CBEIH
 */
public class Resultado {

    private String nome;
    private String data;
    private String path;

    /**
     *
     * @param nome
     * @param dt_ateracao
     * @param path
     */
    public Resultado(String nome, String dt_ateracao, String path) {
        this.nome = nome;
        this.data = dt_ateracao;
        this.path = path;
    }

    /**
     *
     * @return
     */
    public String getNome() {
        return nome;
    }

    /**
     *
     * @param nome
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     *
     * @return
     */
    public String getData() {
        return data;
    }

    /**
     *
     * @param data
     */
    public void setData(String data) {
        this.data = data;
    }

    /**
     *
     * @return
     */
    public String getPath() {
        return path;
    }

    /**
     *
     * @param path
     */
    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return "Resultados{" + "nome=" + nome + ", data=" + data + ", path=" + path + '}';
    }

}
