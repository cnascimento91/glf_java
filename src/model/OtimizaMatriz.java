/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author CBEIH
 */
public class OtimizaMatriz {

    private int N2;
    private int N3;

    private int mMin;
    private int mMax;

    private final int mMaxFinal;
    private final int mMinFinal;

    private int lastZero;

    /**
     *
     * @param N2
     * @param mMin
     */
    public OtimizaMatriz(int N2, int mMin) {
        this.N2 = N2;
        this.N3 = 0;
        this.mMin = mMin;
        this.mMax = 0;
        this.mMaxFinal = mMax;
        this.mMinFinal = mMin;
    }

    /**
     *
     * @return
     */
    public int getN2() {
        return N2;
    }

    /**
     *
     * @param N2
     */
    public void setN2(int N2) {
        this.N2 = N2;
    }

    /**
     *
     * @return
     */
    public int getN3() {
        return N3;
    }

    /**
     *
     * @param N3
     */
    public void setN3(int N3) {
        this.N3 = N3;
    }

    /**
     *
     * @return
     */
    public int getmMin() {
        return mMin;
    }

    /**
     *
     * @param mMin
     */
    public void setmMin(int mMin) {
        this.mMin = mMin;
    }

    /**
     *
     * @return
     */
    public int getmMax() {
        return mMax;
    }

    /**
     *
     * @param mMax
     */
    public void setmMax(int mMax) {
        this.mMax = mMax;
    }

    /**
     *
     * @return
     */
    public int getLastZero() {
        return lastZero;
    }

    /**
     *
     * @param lastZero
     */
    public void setLastZero(int lastZero) {
        this.lastZero = lastZero;
    }

    /**
     *
     * @return
     */
    public int getmMaxFinal() {
        return mMaxFinal;
    }

    /**
     *
     * @return
     */
    public int getmMinFinal() {
        return mMinFinal;
    }

    @Override
    public String toString() {
        return "OtimizaMatriz{" + "N2=" + N2 + ", N3=" + N3 + ", mMin=" + mMin + ", mMax=" + mMax + ", mMaxFinal=" + mMaxFinal + ", mMinFinal=" + mMinFinal + '}';
    }

}
