/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import com.opencsv.CSVReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 *
 * @author CBEIH
 */
public final class Matriz {

    //csv contendo o nome dos arquivos
    private final String matrizRioCSV;
    private final String matrizJusanteMontanteCSV;
    private final String matrizDensidadeCSV;

    private final String URLmatrizRioCSV;
    private final String URLmatrizJusanteMontanteCSV;
    private final String URLmatrizDensidadeCSV;

    //matrizes
    private String[][] MB;
    private String[][] MJM;
    private String[][] MD;

    //ponto maximo do x e y para encerrar uma rodagem
    private int x;
    private int y;

    private final int escala;

    private final int anoInicial;

    //dimensao da matriz
    private int m;
    private int n;

    //SOLUCIONAR
    private Integer[][] MI;
    private Integer[][] MS;
    private Integer[][] MS2030;

    private int nMexilhao;
    private int nCelulasRio;

    private double cellSize;
    private double pixelSize;

    private double dispersaoHidroviaMontante;
    private double dispersaoHidroviaJusante;
    private double dispersaoRioMontante;
    private double dispersaoRioJusante;
    private double dispersaoAfluenteMontante;
    private double dispersaoAfluenteJusante;
    private int distanciaPulo;
    private int distanciaPuloGrande;
    private double quantidadePulo;
    private int iteracaoPorAno;

    private String nome;

    private OtimizaMatriz otimizacao;

    private boolean preProcessada;

    /**
     *
     * @param matrizRioCSV
     * @param matrizJusanteMontanteCSV
     * @param matrizDensidadeCSV
     * @param x
     * @param y
     * @param escala
     * @param anoInicial
     * @param cellSize
     * @param pixelSize
     * @param dispersaoHidroviaMontante
     * @param dispersaoHidroviaJusante
     * @param dispersaoRioMontante
     * @param dispersaoRioJusante
     * @param dispersaoAfluenteMontante
     * @param dispersaoAfluenteJusante
     * @param distanciaPuloGrande
     * @param distanciaPulo
     * @param quantidadePulo
     * @param iteracaoPorAno
     * @param URLmatrizRioCSV
     * @param URLmatrizJusanteMontanteCSV
     * @param URLmatrizDensidadeCSV
     * @param nome
     * @throws IOException
     */
    public Matriz(String matrizRioCSV, String matrizJusanteMontanteCSV, String matrizDensidadeCSV, int x, int y, int escala, int anoInicial, double cellSize, double pixelSize,
            double dispersaoHidroviaMontante, double dispersaoHidroviaJusante, double dispersaoRioMontante,
            double dispersaoRioJusante, double dispersaoAfluenteMontante, double dispersaoAfluenteJusante,
            int distanciaPulo, int distanciaPuloGrande, double quantidadePulo, int iteracaoPorAno,
            String URLmatrizRioCSV, String URLmatrizJusanteMontanteCSV, String URLmatrizDensidadeCSV, String nome) throws IOException {
        this.matrizRioCSV = matrizRioCSV;
        this.matrizJusanteMontanteCSV = matrizJusanteMontanteCSV;
        this.matrizDensidadeCSV = matrizDensidadeCSV;
        this.URLmatrizRioCSV = URLmatrizRioCSV;
        this.URLmatrizJusanteMontanteCSV = URLmatrizJusanteMontanteCSV;
        this.URLmatrizDensidadeCSV = URLmatrizDensidadeCSV;
        this.x = x;
        this.y = y;
        this.escala = escala;
        this.anoInicial = anoInicial;
        this.nome = nome;
        this.cellSize = cellSize;
        this.pixelSize = pixelSize;
        this.dispersaoHidroviaMontante = dispersaoHidroviaMontante;
        this.dispersaoHidroviaJusante = dispersaoHidroviaJusante;
        this.dispersaoRioMontante = dispersaoRioMontante;
        this.dispersaoRioJusante = dispersaoRioJusante;
        this.dispersaoAfluenteMontante = dispersaoAfluenteMontante;
        this.dispersaoAfluenteJusante = dispersaoAfluenteJusante;
        this.distanciaPulo = distanciaPulo;
        this.distanciaPuloGrande = distanciaPuloGrande;
        this.quantidadePulo = quantidadePulo;
        this.iteracaoPorAno = iteracaoPorAno;
        this.preProcessada = false;
    }

    /**
     *
     * @param fileName
     * @param URL
     * @param dir
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static String[][] leMatrizCSV(String fileName, String URL, Diretorio dir) throws FileNotFoundException, IOException {
        if (!dir.checkFileExist(fileName)) {
            Diretorio.gravaArquivoDeURL(URL, (dir.getDiretorioFiles() + "/" + fileName));
        }
        CSVReader csvReader = new CSVReader(new FileReader(new File(dir.getPath() + "/Files/" + fileName)), ';');
        List<String[]> list;
        list = csvReader.readAll();

        // Convert to 2D array
        String[][] dataArr = new String[list.size()][];
        dataArr = list.toArray(dataArr);

        return dataArr;
    }

    /**
     *
     * @return
     */
    public boolean isPreProcessada() {
        return preProcessada;
    }

    /**
     *
     * @param preProcessada
     */
    public void setPreProcessada(boolean preProcessada) {
        this.preProcessada = preProcessada;
    }

    /**
     *
     * @return
     */
    public String getURLmatrizRioCSV() {
        return URLmatrizRioCSV;
    }

    /**
     *
     * @return
     */
    public String getURLmatrizJusanteMontanteCSV() {
        return URLmatrizJusanteMontanteCSV;
    }

    /**
     *
     * @return
     */
    public String getURLmatrizDensidadeCSV() {
        return URLmatrizDensidadeCSV;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    /**
     *
     * @return
     */
    public int getDimensionX() {
        if (MB != null) {
            return this.MB[0].length;
        } else {
            return 0;
        }

    }

    /**
     *
     * @return
     */
    public int getDimensionY() {
        if (MB != null) {
            return MB.length;
        } else {
            return 0;
        }
    }

    /**
     *
     * @param m
     * @return
     */
    public static int getDimensionXMatriz(String[][] m) {
        return m[0].length - 1;
    }

    /**
     *
     * @param m
     * @return
     */
    public static int getDimensionYMatriz(String[][] m) {
        return m.length - 1;
    }

    /**
     *
     * @return
     */
    public String getNome() {
        return nome;
    }

    /**
     *
     * @param nome
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     *
     * @param matriz
     * @return
     */
    public int contaMexilhao(String[][] matriz) {
        int numeroMexilhao = 0;

        int auxLinha = matriz.length;
        int auxColuna = matriz[0].length;

        for (int l = 1; l < auxLinha; l++) {
            for (int c = 1; c < auxColuna; c++) {
                if ("1".equals(matriz[l][c])) {
                    numeroMexilhao += 1;
                } else {
                }
            }
        }

        return numeroMexilhao;

    }

    /**
     *
     * @param dir
     * @throws IOException
     */
    public void preProcessaMatriz(Diretorio dir) throws IOException {

        this.MB = leMatrizCSV(matrizRioCSV, URLmatrizRioCSV, dir);
        this.MJM = leMatrizCSV(matrizJusanteMontanteCSV, URLmatrizJusanteMontanteCSV, dir);
        this.MD = leMatrizCSV(matrizDensidadeCSV, URLmatrizDensidadeCSV, dir);
        this.m = this.getDimensionX();
        this.n = this.getDimensionY();

        this.otimizacao = new OtimizaMatriz(this.n, this.m);
        int soma = 0;
        int numeroMexilhao = 0;

        int auxLinha = this.n;
        int auxColuna = this.m;

        for (int l = 2; l < auxLinha; l++) {
            for (int c = 2; c < auxColuna; c++) {
                if ("0".equals(this.MB[l][c]) || "1".equals(this.MB[l][c]) || "3".equals(this.MB[l][c]) || "5".equals(this.MB[l][c])) {
                    soma += 1;
                    if ("1".equals(this.MB[l][c])) {
                        numeroMexilhao += 1;
                        if (l < this.otimizacao.getN2()) {
                            this.otimizacao.setN2(l);
                            this.otimizacao.setN3(l);
                        }
                    } else if ("0".equals(this.MB[l][c]) || "3".equals(this.MB[l][c]) || "5".equals(this.MB[l][c])) {
                        if (c > this.otimizacao.getmMax()) {
                            this.otimizacao.setmMax(c);
                        } else if (c < this.otimizacao.getmMin()) {
                            this.otimizacao.setmMin(c);
                        }
                    }
                }
            }
        }

        this.nMexilhao = numeroMexilhao;
        this.nCelulasRio = soma;
        setPreProcessada(true);

    }

    /**
     *
     * @throws IOException
     */
    public void limpaMatriz() throws IOException {
        this.MB = null;
        this.MJM = null;
        this.MD = null;
        this.m = 0;
        this.n = 0;
        this.limpaMatrizesResultado();
        this.otimizacao = null;
        this.nMexilhao = 0;
        this.nCelulasRio = 0;
        setPreProcessada(false);
    }

    /**
     *
     * @return
     */
    public boolean salvaCSV() {
        return true;
    }

    /**
     *
     * @return
     */
    public String getMatrizRioCSV() {
        return matrizRioCSV;
    }

    /**
     *
     * @return
     */
    public String getMatrizJusanteMontanteCSV() {
        return matrizJusanteMontanteCSV;
    }

    /**
     *
     * @return
     */
    public String getMatrizDensidadeCSV() {
        return matrizDensidadeCSV;
    }

    /**
     *
     * @return
     */
    public String[][] getMB() {
        return MB;
    }

    /**
     *
     * @return
     */
    public String[][] getMJM() {
        return MJM;
    }

    /**
     *
     * @return
     */
    public String[][] getMD() {
        return MD;
    }

    /**
     *
     * @return
     */
    public int getX() {
        return x;
    }

    /**
     *
     * @return
     */
    public int getY() {
        return y;
    }

    /**
     *
     * @return
     */
    public int getEscala() {
        return escala;
    }

    /**
     *
     * @return
     */
    public int getAnoInicial() {
        return anoInicial;
    }

    /**
     *
     * @return
     */
    public int getM() {
        return m;
    }

    /**
     *
     * @return
     */
    public int getN() {
        return n;
    }

    /**
     *
     * @param MI
     */
    public void setMI(Integer[][] MI) {
        this.MI = MI;
    }

    /**
     *
     * @param MS
     */
    public void setMS(Integer[][] MS) {
        this.MS = MS;
    }
    
    /**
     *
     * @param MS2030
     */
    public void setMS2030(Integer[][] MS2030) {
        this.MS2030 = MS2030;
    }

    /**
     *
     * @return
     */
    public Integer[][] getMI() {
        return MI;
    }

    /**
     *
     * @return
     */
    public Integer[][] getMS() {
        return MS;
    }
    
    /**
     *
     * @return
     */
    public Integer[][] getMS2030() {
        return MS2030;
    }

    /**
     *
     * @return
     */
    public OtimizaMatriz getOtimizacao() {
        return otimizacao;
    }

    /**
     *
     * @param otimizacao
     */
    public void setOtimizacao(OtimizaMatriz otimizacao) {
        this.otimizacao = otimizacao;
    }

    /**
     *
     * @return
     */
    public String[][] copyMB() {
        if (MB != null) {
            String[][] cpMB = new String[MB.length][];
            for (int i = 0; i < MB.length; i++) {
                System.arraycopy(MB[i], 0, cpMB[i], 0, MB[i].length);

            }
            return cpMB;
        } else {
            return null;
        }

    }

    /**
     *
     * @return
     */
    public int getnMexilhao() {
        return nMexilhao;
    }

    /**
     *
     * @param nMexilhao
     */
    public void setnMexilhao(int nMexilhao) {
        this.nMexilhao = nMexilhao;
    }

    /**
     *
     * @return
     */
    public int getnCelulasRio() {
        return nCelulasRio;
    }

    /**
     *
     * @param nCelulasRio
     */
    public void setnCelulasRio(int nCelulasRio) {
        this.nCelulasRio = nCelulasRio;
    }

    /**
     *
     * @return
     */
    public double getCellSize() {
        return cellSize;
    }

    /**
     *
     * @param cellSize
     */
    public void setCellSize(double cellSize) {
        this.cellSize = cellSize;
    }

    /**
     *
     * @return
     */
    public double getPixelSize() {
        return pixelSize;
    }

    /**
     *
     * @param pixelSize
     */
    public void setPixelSize(double pixelSize) {
        this.pixelSize = pixelSize;
    }

    /**
     *
     * @return
     */
    public double getDispersaoHidroviaMontante() {
        return dispersaoHidroviaMontante;
    }

    /**
     *
     * @param dispersaoHidroviaMontante
     */
    public void setDispersaoHidroviaMontante(double dispersaoHidroviaMontante) {
        this.dispersaoHidroviaMontante = dispersaoHidroviaMontante;
    }

    /**
     *
     * @return
     */
    public double getDispersaoHidroviaJusante() {
        return dispersaoHidroviaJusante;
    }

    /**
     *
     * @param dispersaoHidroviaJusante
     */
    public void setDispersaoHidroviaJusante(double dispersaoHidroviaJusante) {
        this.dispersaoHidroviaJusante = dispersaoHidroviaJusante;
    }

    /**
     *
     * @return
     */
    public double getDispersaoRioMontante() {
        return dispersaoRioMontante;
    }

    /**
     *
     * @param dispersaoRioMontante
     */
    public void setDispersaoRioMontante(double dispersaoRioMontante) {
        this.dispersaoRioMontante = dispersaoRioMontante;
    }

    /**
     *
     * @return
     */
    public double getDispersaoRioJusante() {
        return dispersaoRioJusante;
    }

    /**
     *
     * @param dispersaoRioJusante
     */
    public void setDispersaoRioJusante(double dispersaoRioJusante) {
        this.dispersaoRioJusante = dispersaoRioJusante;
    }

    /**
     *
     * @return
     */
    public double getDispersaoAfluenteMontante() {
        return dispersaoAfluenteMontante;
    }

    /**
     *
     * @param dispersaoAfluenteMontante
     */
    public void setDispersaoAfluenteMontante(double dispersaoAfluenteMontante) {
        this.dispersaoAfluenteMontante = dispersaoAfluenteMontante;
    }

    /**
     *
     * @return
     */
    public double getDispersaoAfluenteJusante() {
        return dispersaoAfluenteJusante;
    }

    /**
     *
     * @param dispersaoAfluenteJusante
     */
    public void setDispersaoAfluenteJusante(double dispersaoAfluenteJusante) {
        this.dispersaoAfluenteJusante = dispersaoAfluenteJusante;
    }

    /**
     *
     * @return
     */
    public int getDistanciaPulo() {
        return distanciaPulo;
    }

    /**
     *
     * @param distanciaPulo
     */
    public void setDistanciaPulo(int distanciaPulo) {
        this.distanciaPulo = distanciaPulo;
    }

    /**
     *
     * @return
     */
    public int getDistanciaPuloGrande() {
        return distanciaPuloGrande;
    }

    /**
     *
     * @param distanciaPuloGrande
     */
    public void setDistanciaPuloGrande(int distanciaPuloGrande) {
        this.distanciaPuloGrande = distanciaPuloGrande;
    }

    /**
     *
     * @return
     */
    public double getQuantidadePulo() {
        return quantidadePulo;
    }

    /**
     *
     * @param quantidadePulo
     */
    public void setQuantidadePulo(double quantidadePulo) {
        this.quantidadePulo = quantidadePulo;
    }

    /**
     *
     * @return
     */
    public int getIteracaoPorAno() {
        return iteracaoPorAno;
    }

    /**
     *
     * @param iteracaoPorAno
     */
    public void setIteracaoPorAno(int iteracaoPorAno) {
        this.iteracaoPorAno = iteracaoPorAno;
    }

    private void limpaMatrizesResultado() {
        this.MI = null;
        this.MS = null;
        this.MS2030 = null;
    }

    /**
     *
     * @param diretorio
     * @param nomeExecucao
     * @throws IOException
     */
    public void gravaMatrizesResultado(String diretorio, String nomeExecucao) throws IOException {
        if (Diretorio.criarDiretorio(diretorio)) {
            //Escrita dos dados da tabela
            //try ( //Criação de um buffer para a escrita em uma stream
            //        BufferedWriter StrW = new BufferedWriter(new FileWriter(diretorio + "/" + nomeExecucao + "_MatrizIteracao.csv"))) {
                //Escrita dos dados da tabela
            //    for (int l = 0; l < this.getDimensionY(); l++) {
            //        for (int c = 0; c < this.getDimensionX(); c++) {
            //            StrW.write(Integer.toString(this.getMI()[l][c]));
            //            StrW.write(";");
            //        }
            //        StrW.write("\n");
            //    }

            //} catch (FileNotFoundException ex) {
            //} catch (IOException e) {
            //}
            try ( //Criação de um buffer para a escrita em uma stream
                    BufferedWriter StrW3 = new BufferedWriter(new FileWriter(diretorio + "/" + nomeExecucao + "_MatrizQuantidade_2030.csv"))) {
                //Escrita dos dados da tabela
                for (int l = 0; l < this.getDimensionY(); l++) {
                    for (int c = 0; c < this.getDimensionX(); c++) {
                        StrW3.write(Integer.toString(this.getMS2030()[l][c]));
                        StrW3.write(";");
                    }
                    StrW3.write("\n");
                }

            } catch (FileNotFoundException ex) {
            } catch (IOException e) {
            }
            try ( //Criação de um buffer para a escrita em uma stream
                    BufferedWriter StrW2 = new BufferedWriter(new FileWriter(diretorio + "/" + nomeExecucao + "_MatrizQuantidade.csv"))) {
                //Escrita dos dados da tabela
                for (int l = 0; l < this.getDimensionY(); l++) {
                    for (int c = 0; c < this.getDimensionX(); c++) {
                        StrW2.write(Integer.toString(this.getMS()[l][c]));
                        StrW2.write(";");
                    }
                    StrW2.write("\n");
                }

            } catch (FileNotFoundException ex) {
            } catch (IOException e) {
            }

        }
    }

    static public void gravaMatrizLocal(String diretorio, String nomeArquivo, String[][] matriz) throws IOException {

        int x, y;

        y = matriz.length - 1;
        x = matriz[0].length - 1;

        if (Diretorio.criarDiretorio(diretorio)) {
            //Escrita dos dados da tabela
            try ( //Criação de um buffer para a escrita em uma stream
                    BufferedWriter StrW = new BufferedWriter(new FileWriter(diretorio + "/" + "_user_" + nomeArquivo + ".csv"))) {
                //Escrita dos dados da tabela
                for (int l = 0; l < y; l++) {
                    for (int c = 0; c < x; c++) {
                        StrW.write(matriz[l][c]);
                        StrW.write(";");
                    }
                    StrW.write("\n");
                }

            } catch (FileNotFoundException ex) {
            } catch (IOException e) {
            }
        }
    }

    static public boolean verificaPontoChegada(String[][] matriz, int x, int y, int distancia) {
        int n = matriz.length-1;
        int m = matriz[0].length-1;
        int i2 = y - distancia;
        int i3 = y + distancia;
        int j2 = x - distancia;
        int j3 = x + distancia;
        if (i2 < 0) {
            i2 = 0;
        }
        if (i3 > n) {
            i3 = n;
        }
        if (j2 < 0) {
            j2 = 0;
        }
        if (j3 > m) {
            j3 = m;
        }

        for (int l = i2; l <= i3; l++) {
            for (int c = j2; c <= j3; c++) {              
                if ("1".equals(matriz[l][c])) {
                    return true;
                }
            }
        }

        return false;
    }

    public boolean isFinalPointDefined() {
        return (this.x != 0 && this.y != 0);
    }

    @Override
    public String toString() {
        return "Matriz{" + "matrizRioCSV=" + matrizRioCSV + ", matrizJusanteMontanteCSV=" + matrizJusanteMontanteCSV + ", matrizDensidadeCSV=" + matrizDensidadeCSV + ", URLmatrizRioCSV=" + URLmatrizRioCSV + ", URLmatrizJusanteMontanteCSV=" + URLmatrizJusanteMontanteCSV + ", URLmatrizDensidadeCSV=" + URLmatrizDensidadeCSV + ", MB=" + MB + ", MJM=" + MJM + ", MD=" + MD + ", x=" + x + ", y=" + y + ", escala=" + escala + ", anoInicial=" + anoInicial + ", m=" + m + ", n=" + n + ", MI=" + MI + ", MS=" + MS + ", nMexilhao=" + nMexilhao + ", nCelulasRio=" + nCelulasRio + ", cellSize=" + cellSize + ", pixelSize=" + pixelSize + ", dispersaoHidroviaMontante=" + dispersaoHidroviaMontante + ", dispersaoHidroviaJusante=" + dispersaoHidroviaJusante + ", dispersaoRioMontante=" + dispersaoRioMontante + ", dispersaoRioJusante=" + dispersaoRioJusante + ", dispersaoAfluenteMontante=" + dispersaoAfluenteMontante + ", dispersaoAfluenteJusante=" + dispersaoAfluenteJusante + ", distanciaPulo=" + distanciaPulo + ", distanciaPuloGrande=" + distanciaPuloGrande + ", quantidadePulo=" + quantidadePulo + ", iteracaoPorAno=" + iteracaoPorAno + ", nome=" + nome + ", otimizacao=" + otimizacao + ", preProcessada=" + preProcessada + '}';
    }

}
