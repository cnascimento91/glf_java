package util;

import java.io.IOException;
import java.sql.*;
import controller.Programa;

/**
 * Responsible for the actions and connection with the database SQLite
 *
 * @author robertoroquesilva
 */
public class SqlLiteConexao {

    private Connection conn;

    /**
     * Create the connections in the specific path.
     *
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws java.io.IOException
     */
    public SqlLiteConexao() throws SQLException, ClassNotFoundException, IOException {

        //Diretorio dir = new Diretorio();
        //dir.downloadDBFromServer("glf.db");
        //Class.forName("org.sqlite.JDBC");
        //this.conn = DriverManager.getConnection("jdbc:sqlite:" + dir.getPath() + "/glf.db");
    }

    public SqlLiteConexao(Programa programa) throws SQLException, ClassNotFoundException, IOException {
        //com conexao com internet
        if (programa.isConexaoInternet()) {
            programa.setStatusDB(1);
            programa.getDir().downloadDBFromServer("glf.db");
            Class.forName("org.sqlite.JDBC");
            this.conn = DriverManager.getConnection("jdbc:sqlite:" + programa.getDir().getDiretorioDB() + "/glf.db");
        } // sem conexao com internet
        else // existe database offline
         if (programa.getDir().checkDBExist("glf.db")) {
                programa.setStatusDB(2);
                Class.forName("org.sqlite.JDBC");
                this.conn = DriverManager.getConnection("jdbc:sqlite:" + programa.getDir().getDiretorioDB() + "/glf.db");
            } //nao existe database offline
            else {
                programa.setStatusDB(0);
                Class.forName("org.sqlite.JDBC");
                this.conn = DriverManager.getConnection("jdbc:sqlite:" + programa.getDir().getDiretorioDB() + "/glf.db");
                initDB();
                insereDadosIniciais();
            }
    }

    /**
     * Initialize the database creating the tables
     *
     * @throws SQLException
     */
    public void initDB() throws SQLException {
        Statement stm = this.conn.createStatement();

        stm.execute("CREATE TABLE IF NOT EXISTS `glf` ("
                + "`Nome` varchar(100) NOT NULL,"
                + "`SourceHidrografia` varchar(100) NOT NULL,"
                + "`sourceAltitude` varchar(100) NOT NULL,"
                + "`sourceDensidade` varchar(100) NOT NULL,"
                + "`id` int(11) NOT NULL PRIMARY KEY ,"
                + "`URLHidrografia` varchar(400) NOT NULL,"
                + "`URLAltitude` varchar(400) NOT NULL,"
                + "`URLDensidade` varchar(400) NOT NULL,"
                + "`anoInicial` int(11) NOT NULL,"
                + "`coordenadaFinalX` int(11) NOT NULL,"
                + "`coordenadaFinalY` int(11) NOT NULL,"
                + "`escala` int(11) NOT NULL,"
                + "`cellSize` double NOT NULL,"
                + "`pixelSize` double NOT NULL,"
                + "`dispersaoHidroviaMontante` double NOT NULL,"
                + "`dispersaoHidroviaJusante` double NOT NULL,"
                + "`dispersaoRioMontante` double NOT NULL,"
                + "`dispersaoRioJusante` double NOT NULL,"
                + "`dispersaoAfluenteMontante` double NOT NULL,"
                + "`dispersaoAfluenteJusante` double NOT NULL,"
                + "`distanciaPulo` int(11) NOT NULL,"
                + "`distanciaPuloGrande` int(11) NOT NULL,"
                + "`quantidadePulo` double NOT NULL,"
                + "`iteracaoPorAno` int(11) NOT NULL)");
    }

    public void insereDadosIniciais() throws SQLException {
        Statement stm = this.conn.createStatement();

        String sql = "INSERT INTO `glf` (`Nome`, `SourceHidrografia`, `sourceAltitude`, `sourceDensidade`, `id`, `URLHidrografia`,"
                + "`URLAltitude`, `URLDensidade`, `anoInicial`, `coordenadaFinalX`, `coordenadaFinalY`, `escala`, `cellSize`,"
                + "`pixelSize`, `dispersaoHidroviaMontante`, `dispersaoHidroviaJusante`, `dispersaoRioMontante`, `dispersaoRioJusante`,"
                + "`dispersaoAfluenteMontante`, `dispersaoAfluenteJusante`, `distanciaPulo`, `distanciaPuloGrande`, `quantidadePulo`, "
                + "`iteracaoPorAno`) VALUES('Brasil Pos-2016', 'brasil_4km_NEW.csv', 'brasil_4km_altitude_NEW.csv', 'brasil_4km_2015_densidade.csv', 0, "
                + "'http://www.cbeih.org/GLf/Files/brasil_4km_NEW.csv',"
                + "'http://www.cbeih.org/GLf/Files/brasil_4km_altitude_NEW.csv',"
                + "'http://www.cbeih.org/GLf/Files/brasil_4km_2015_densidade.csv',"
                + "2016, 273, 47, 25, 1, 1, 0.125, 0.125, 0.95, 0.25, 0.99, 0.5, 15, 100, 0.4, 400),"
                + "('Brasil Pos-1998', 'brasil_4km_98_NEW.csv', 'brasil_4km_altitude_NEW.csv', 'brasil_4km_2015_densidade.csv',"
                + "1, 'http://www.cbeih.org/GLf/Files/brasil_4km_98_NEW.csv',"
                + "'http://www.cbeih.org/GLf/Files/brasil_4km_altitude_NEW.csv',"
                + "'http://www.cbeih.org/GLf/Files/brasil_4km_2015_densidade.csv', 1998, 273, 47, 25, 1, 1, 0.125, 0.125, 0.95, 0.25, 0.99,"
                + "0.5, 15, 100, 0.4, 400);";

        stm.execute(sql);
    }

    /**
     * Execute a SQL statement of the insert or update type
     *
     * @param sql
     * @return result as int
     * @throws SQLException
     */
    public int executeSQL(String sql) throws SQLException {
        Statement stm = this.conn.createStatement();
        return stm.executeUpdate(sql);
    }

    /**
     * Execute a select command in the database
     *
     * @param sql
     * @return ResultSet
     * @throws SQLException
     */
    public ResultSet executeQuerySQL(String sql) throws SQLException {
        Statement stm = this.conn.createStatement();
        return stm.executeQuery(sql);
    }

    /**
     *
     * @throws SQLException
     */
    public void close() throws SQLException {
        conn.close();
    }

}
