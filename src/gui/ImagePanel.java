/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import controller.Programa;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author CBEIH
 */
public class ImagePanel extends JPanel {

    private final BufferedImage image;
    private String[][] matrizHidrografia;
    private Integer[][] matrizResultado;
    private final int limite;
    private int yLast;
    private int xLast;
    private final Programa programa;
    private String celulaLast;
    private TelaDefinirChegada tela;
    private Integer[][] matrizMediaAno;
    private int areaChegada;
    private int tempoChegadaTotal;

    /**
     *
     * @param width
     * @param height
     * @param image
     * @param dataArrH
     * @param dataArrR
     * @param limite
     * @param programa
     * @param tela
     */
    public ImagePanel(int width, int height, BufferedImage image, String[][] dataArrH, Integer[][] dataArrR, int limite, Programa programa, TelaDefinirChegada tela) {
        this.image = image;
        this.limite = limite;
        this.matrizHidrografia = dataArrH;
        this.matrizResultado = dataArrR;
        this.programa = programa;
        this.tela = tela;
        this.matrizMediaAno = null;
        this.tempoChegadaTotal = 0;

        if (matrizResultado == null) {

            addMouseListener(new java.awt.event.MouseAdapter() {
                @Override
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    formMouseClicked(evt);
                }
            });

        }
    }

    private void formMouseClicked(java.awt.event.MouseEvent evt) {
        int x = evt.getX();
        int y = evt.getY();
        procuraCelulaRioProxima(x, y);
        if (this.tela != null) {
            int opt = JOptionPane.showConfirmDialog(tela, "Voce tem certeza?");
            if (opt == 0) {
                programa.setParadaY(this.yLast);
                programa.setParadaX(this.xLast);
                tela.dispose();
            } else {
                clean();
            }
        }
    }

    /**
     *
     * @return
     */
    public String[][] getMatrizHidrografia() {
        return matrizHidrografia;
    }

    /**
     *
     * @return
     */
    public Integer[][] getMatrizResultado() {
        return matrizResultado;
    }

    public Integer[][] getMatrizMediaAno() {
        return matrizMediaAno;
    }

    public void setMatrizMediaAno(Integer[][] matrizMediaAno) {
        this.matrizMediaAno = matrizMediaAno;
    }

    public int getAreaChegada() {
        return areaChegada;
    }

    public void setAreaChegada(int areaChegada) {
        this.areaChegada = areaChegada;
    }

    public int getyLast() {
        return yLast;
    }

    public int getxLast() {
        return xLast;
    }

    public void setTempoChegadaTotal(int ano) {
        this.tempoChegadaTotal = ano;
    }

    /**
     *
     * @param matriz
     */
    public void setMatrizH(String[][] matriz) {
        this.matrizHidrografia = matriz;
        repaint();
    }

    /**
     *
     * @param matriz
     */
    public void setMatrizR(Integer[][] matriz) {
        this.matrizResultado = matriz;
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        for (int i = 0; i < image.getHeight(); i++) {
            for (int j = 0; j < image.getWidth(); j++) {
                if (null == matrizHidrografia[i][j]) {
                } else {
                    switch (matrizHidrografia[i][j]) {
                        case "1":
                            image.setRGB(j, i - 1, new Color(255, 0, 0).getRGB());
                            image.setRGB(j - 1, i, new Color(255, 0, 0).getRGB());
                            image.setRGB(j, i + 1, new Color(255, 0, 0).getRGB());
                            image.setRGB(j, i, new Color(255, 0, 0).getRGB());
                            image.setRGB(j + 1, i, new Color(255, 0, 0).getRGB());
                            image.setRGB(j - 1, i - 1, new Color(255, 0, 0).getRGB());
                            image.setRGB(j + 1, i - 1, new Color(255, 0, 0).getRGB());
                            image.setRGB(j + 1, i + 1, new Color(255, 0, 0).getRGB());
                            image.setRGB(j - 1, i + 1, new Color(255, 0, 0).getRGB());
                            break;
                        case "2":
                            if (programa.getParadaX() != 0 && programa.getParadaY() != 0) {
                                int i2 = programa.getParadaY() - areaChegada;
                                int i3 = programa.getParadaY() + areaChegada;
                                int j2 = programa.getParadaX() - areaChegada;
                                int j3 = programa.getParadaX() + areaChegada;
                                if (i2 < 0) {
                                    i2 = 0;
                                }
                                if (i3 > image.getHeight()) {
                                    i3 = image.getHeight();
                                }
                                if (j2 < 0) {
                                    j2 = 0;
                                }
                                if (j3 > image.getWidth()) {
                                    j3 = image.getWidth();
                                }
                                if ((i > i2 && i < i3) && (j > j2 && j < j3) && i != 0 && j != 0) {
                                    image.setRGB(j, i, new Color(253, 233, 16).getRGB());
                                    break;
                                } else {
                                    image.setRGB(j, i, new Color(0, 0, 0).getRGB());
                                    break;
                                }
                            } else {
                                image.setRGB(j, i, new Color(0, 0, 0).getRGB());
                                break;
                            }

                        case "5":
                            image.setRGB(j, i, new Color(0, 0, 255).getRGB());
                            break;
                        case "3":
                            image.setRGB(j, i, new Color(173, 216, 230).getRGB());
                            break;
                        case "0":
                            image.setRGB(j, i, new Color(0, 191, 255).getRGB());
                            break;
                        default:
                            break;
                    }
                }

            }
        }

        if (matrizResultado != null) {
            for (int i = 0; i < image.getHeight(); i++) {
                for (int j = 0; j < image.getWidth(); j++) {
                    if (null == matrizResultado[i][j]) {
                    }
                    if (matrizResultado[i][j] > 0 && matrizResultado[i][j] <= (limite / 7 * 1)) {
                        image.setRGB(j, i, new Color(255, 160, 122).getRGB());
                    } else if (matrizResultado[i][j] > (limite / 7 * 1) && matrizResultado[i][j] <= (limite / 7 * 2)) {
                        image.setRGB(j, i, new Color(233, 150, 122).getRGB());
                    } else if (matrizResultado[i][j] > (limite / 7 * 2) && matrizResultado[i][j] <= (limite / 7 * 3)) {
                        image.setRGB(j, i, new Color(250, 128, 114).getRGB());
                    } else if (matrizResultado[i][j] > (limite / 7 * 3) && matrizResultado[i][j] <= (limite / 7 * 4)) {
                        image.setRGB(j, i, new Color(165, 42, 42).getRGB());
                    } else if (matrizResultado[i][j] > (limite / 7 * 4) && matrizResultado[i][j] <= (limite / 7 * 5)) {
                        image.setRGB(j, i, new Color(178, 34, 34).getRGB());
                    } else if (matrizResultado[i][j] > (limite / 7 * 5) && matrizResultado[i][j] <= (limite / 7 * 6)) {
                        image.setRGB(j, i, new Color(139, 0, 0).getRGB());
                    } else if (matrizResultado[i][j] > (limite / 7 * 6) && matrizResultado[i][j] <= (limite / 7 * 7)) {
                        image.setRGB(j, i, new Color(128, 0, 0).getRGB());
                    } else if (matrizResultado[i][j] > (limite / 7 * 7)) {
                        image.setRGB(j, i, new Color(128, 0, 0).getRGB());
                    }
                    //else if(i == programa.getParadaY() && j == programa.getParadaX() && i != 0 && j != 0){
                    //   for (int a = i-15; a < i+15; a++) {
                    //        for (int b = j-15; b < j+15; b++) {
                    //            image.setRGB(b, a, new Color(253, 233, 16).getRGB());
                    //        }
                    //    }  
                    //}

                }
            }
        }
        g.drawImage(image, 0, 0, getWidth(), getHeight(), this);
        if (matrizMediaAno != null) {
            g.setColor(Color.red);
            g.setFont(new Font("Serif", Font.BOLD, 35));
            String s;
            int ano = (this.tempoChegadaTotal / this.limite);
            if (ano < 1) {
                s = "< 1 ano";
                g.drawString(s, 170, 650);
            } else if (ano == 1) {
                s = " ano";
                g.drawString(Integer.toString(ano) + s, 160, 650);
            } else {
                s = " anos";
                g.drawString(Integer.toString(ano) + s, 160, 650);
            }
        }

    }

    /**
     *
     * @param component
     * @param format
     * @param output
     * @throws IOException
     */
    public static void geraImagemResultado(Component component, String format, OutputStream output) throws IOException {
        // criar imagem em memória  
        int width = component.getWidth();
        int height = component.getHeight();
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

        // obter contexto gráfico da imagem  
        Graphics graphics = image.getGraphics();

        // desenhar o componente no contexto grafico  
        component.paintAll(graphics);
        graphics.dispose();

        // salvar imagem  
        ImageIO.write(image, format, output);
    }

    public String getCelulaLast() {
        return celulaLast;
    }

    public void setCelulaLast(String celulaLast) {
        this.celulaLast = celulaLast;
    }

    private void procuraCelulaRioProxima(int x, int y) {

        int coluna;

        coluna = this.matrizHidrografia[0].length - 1;

        int auxMenor = coluna - 1, auxMaior = coluna - 1, y2, x2;

        y2 = (int) (y * 1.155);
        for (int c = 0; c < coluna; c++) {
            if ("0".equals(this.matrizHidrografia[y2][c]) || "3".equals(this.matrizHidrografia[y2][c]) || "5".equals(this.matrizHidrografia[y2][c])) {
                
                if (c < x) {
                    auxMenor = c;
                } else {
                    auxMaior = c;
                    c = coluna;
                    break;
                }
            }
        }

        if (Math.abs(x - auxMenor) > Math.abs(auxMaior - x)) {
            x2 = auxMaior;
        } else {
            x2 = auxMenor;
        }

        this.xLast = x2;
        this.yLast = y2;
        this.celulaLast = this.matrizHidrografia[(y2)][x2];
        this.matrizHidrografia[(y2)][x2] = "1";
        this.setMatrizH(this.matrizHidrografia);

    }

    public void clean() {

        String vCelulas[];
        vCelulas = new String[8];
        for (int l = 0; l < this.matrizHidrografia.length; l++) {
            for (int c = 0; c < this.matrizHidrografia[0].length; c++) {
                if ("1".equals(this.matrizHidrografia[l][c])) {
                    vCelulas[0] = this.matrizHidrografia[l - 1][c - 1];
                    vCelulas[1] = this.matrizHidrografia[l - 1][c];
                    vCelulas[2] = this.matrizHidrografia[l - 1][c + 1];
                    vCelulas[3] = this.matrizHidrografia[l][c - 1];
                    vCelulas[4] = this.matrizHidrografia[l][c + 1];
                    vCelulas[5] = this.matrizHidrografia[l + 1][c - 1];
                    vCelulas[6] = this.matrizHidrografia[l + 1][c];
                    vCelulas[7] = this.matrizHidrografia[l + 1][c + 1];
                    for (int i = 0; i < 8; i++) {
                        if ("5".equals(vCelulas[i])) {
                            this.matrizHidrografia[l][c] = vCelulas[i];
                            i = 8;
                            break;
                        } else if ("0".equals(vCelulas[i])) {
                            this.matrizHidrografia[l][c] = vCelulas[i];
                        } else if ("3".equals(vCelulas[i]) && !"0".equals(this.matrizHidrografia[l][c])) {
                            this.matrizHidrografia[l][c] = vCelulas[i];
                        }
                    }
                }
            }
        }
        repaint();
    }

}
