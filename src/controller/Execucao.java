/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import gui.TelaExecucao;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import model.Diretorio;
import model.Matriz;

/**
 *
 * @author CBEIH
 */
public class Execucao {

    private double media;
    private String[][] A;
    private String[][] MA;

    private Integer[][] MI;
    private Integer[][] MS2030;
    private Integer[][] MS;

    private int limiteExecucao;
    private int nIteracao;
    private int limiteAno;
    private Matriz matriz;

    private int execucao;

    private int ano;
    private int month;
    private String anoMes;

    private final String home;
    private final String diretorio;
    private final String diretorioResultados;

    private final String nomeExecucao;

    private int tipo;
    private int chegada;
    private int areaChegada;
    private int tempoTotal;

    /**
     *
     * @param dir
     */
    public Execucao(Diretorio dir) {
        this.nomeExecucao = this.getDateTime();
        this.execucao = 0;
        this.nIteracao = 0;
        this.home = dir.findHome();
        this.diretorio = dir.getPath();
        this.diretorioResultados = dir.getDiretorioResultados();
        this.chegada = 0;
        this.tempoTotal = 0;
    }

    /**
     *
     * @param nomeExecucao
     * @param dir
     */
    public Execucao(String nomeExecucao, Diretorio dir) {
        this.nomeExecucao = nomeExecucao;
        this.execucao = 0;
        this.nIteracao = 0;
        this.home = dir.findHome();
        this.diretorio = dir.getPath();
        this.diretorioResultados = dir.getDiretorioResultados();
        this.chegada = 0;
        this.tempoTotal = 0;
    }

    /**
     *
     */
    public void limpaExecucao() {
        this.A = null;
        this.MA = null;
        this.nIteracao = 0;
        this.matriz = null;
        this.ano = 0;
        this.month = 0;
        this.anoMes = "";
    }

    /**
     *
     */
    public void initAandMA() {
        this.A = new String[this.matriz.getDimensionY()][this.matriz.getDimensionX()];
        this.MA = new String[this.matriz.getDimensionY()][this.matriz.getDimensionX()];
    }

    /**
     *
     */
    public void iniciaMatrizesResultado() {
        int auxLinha = this.matriz.getN();
        int auxColuna = this.matriz.getM();

        this.MI = new Integer[auxLinha][auxColuna];
        this.MS = new Integer[auxLinha][auxColuna];
        this.MS2030 = new Integer[auxLinha][auxColuna];

        for (int l = 0; l < auxLinha; l++) {
            for (int c = 0; c < auxColuna; c++) {
                this.MI[l][c] = 0;
                this.MS[l][c] = 0;
                this.MS2030[l][c] = 0;
            }
        }

        this.matriz.setMI(MI);
        this.matriz.setMS(MS);
        this.matriz.setMS2030(MS2030);
    }

    /**
     *
     * @return
     */
    public Integer[][] getMI() {
        return MI;
    }

    /**
     *
     * @param MI
     */
    public void setMI(Integer[][] MI) {
        this.MI = MI;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public int getChegada() {
        return chegada;
    }

    public void setChegada(int chegada) {
        this.chegada = chegada;
    }

    public int getAreaChegada() {
        return areaChegada;
    }

    public int getTempoTotal() {
        return tempoTotal;
    }

    public void addTempoTotal(int tempoTotal) {
        this.tempoTotal += tempoTotal;
    }

    public void setAreaChegada(int areaChegada) {
        this.areaChegada = areaChegada;
    }

    /**
     *
     * @return
     */
    public Integer[][] getMS() {
        return MS;
    }

    /**
     *
     * @param MS
     */
    public void setMS(Integer[][] MS) {
        this.MS = MS;
    }
    
        /**
     *
     * @return
     */
    public Integer[][] getMS2030() {
        return MS2030;
    }

    /**
     *
     * @param MS2030
     */
    public void setMS2030(Integer[][] MS2030) {
        this.MS2030 = MS2030;
    }

    /**
     *
     * @return
     */
    public double getMedia() {
        return media;
    }

    /**
     *
     * @param media
     */
    public void setMedia(double media) {
        this.media = media;
    }

    /**
     *
     * @return
     */
    public String[][] getA() {
        return A;
    }

    /**
     *
     * @param A
     */
    public void setA(String[][] A) {
        this.A = A;
    }

    /**
     *
     * @return
     */
    public String[][] getMA() {
        return MA;
    }

    /**
     *
     * @param MA
     */
    public void setMA(String[][] MA) {
        this.MA = MA;
    }

    /**
     *
     * @return
     */
    public Matriz getM() {
        return matriz;
    }

    public int getLimiteExecucao() {
        return limiteExecucao;
    }

    /**
     *
     * @param limiteExecucao
     */
    public void setLimiteExecucao(int limiteExecucao) {
        this.limiteExecucao = limiteExecucao;
    }

    /**
     *
     * @return
     */
    public int getLimiteAno() {
        return limiteAno;
    }

    /**
     *
     * @param limiteAno
     */
    public void setLimiteAno(int limiteAno) {
        this.limiteAno = limiteAno;
    }

    /**
     *
     * @return
     */
    public Matriz getMatriz() {
        return matriz;
    }

    /**
     *
     * @param matriz
     */
    public void setMatriz(Matriz matriz) {
        this.matriz = matriz;
    }

    /**
     *
     * @return
     */
    public int getAno() {
        return ano;
    }

    /**
     *
     * @param ano
     */
    public void setAno(int ano) {
        this.ano = ano;
    }

    /**
     *
     * @return
     */
    public int getMonth() {
        return month;
    }

    /**
     *
     * @param month
     */
    public void setMonth(int month) {
        this.month = month;
    }

    /**
     *
     * @return
     */
    public String getAnoMes() {
        return anoMes;
    }

    /**
     *
     * @param anoMes
     */
    public void setAnoMes(String anoMes) {
        this.anoMes = anoMes;
    }

    /**
     *
     * @return
     */
    public int getExecucao() {
        return execucao;
    }

    /**
     *
     * @return
     */
    public String getNomeExecucao() {
        return nomeExecucao;
    }

    /**
     *
     */
    public void addExecucao() {
        this.execucao += 1;
    }

    /**
     *
     * @return
     */
    public int getLimite() {
        return limiteExecucao;
    }

    /**
     *
     * @param limite
     */
    public void setLimite(int limite) {
        this.limiteExecucao = limite;
    }

    /**
     *
     * @return
     */
    public int getnIteracao() {
        return nIteracao;
    }

    /**
     *
     */
    public void addnIteracao() {
        this.nIteracao += 1;
    }

    /**
     *
     */
    public void copyMAtoA() {
        for (int l = 0; l < this.MA.length; l++) {
            System.arraycopy(this.MA[l], 0, A[l], 0, this.MA[0].length);
        }
    }

    /**
     *
     */
    public void copyMBtoA() {
        for (int l = 0; l < this.matriz.getMB().length; l++) {
            System.arraycopy(this.matriz.getMB()[l], 0, A[l], 0, this.matriz.getMB()[0].length);
        }
    }

    /**
     *
     */
    public void copyMBtoMA() {
        for (int l = 0; l < this.matriz.getMB().length; l++) {
            System.arraycopy(this.matriz.getMB()[l], 0, MA[l], 0, this.matriz.getMB()[0].length);
        }
    }

    /**
     *
     * @param nIteracao
     * @return
     */
    public String calculaAnoMes(int nIteracao) {
        String y, m, e, yStr, mStr;

        nIteracao = nIteracao * this.matriz.getEscala();
        this.ano = nIteracao / this.matriz.getIteracaoPorAno();
        this.month = 12 * (nIteracao - this.ano * (this.matriz.getIteracaoPorAno())) / this.matriz.getIteracaoPorAno();

        switch (this.ano) {
            case 0:
                y = " ";
                yStr = " ";
                break;
            case 1:
                y = " year ";
                yStr = Integer.toString(this.ano);
                break;
            default:
                y = " years ";
                yStr = Integer.toString(this.ano);
                break;
        }

        switch (this.month) {
            case 0:
                m = " ";
                mStr = " ";
                break;
            case 1:
                m = " month ";
                mStr = Integer.toString(this.month);
                break;
            default:
                m = " months ";
                mStr = Integer.toString(this.month);
                break;
        }

        if (this.month != 0 && this.ano != 0) {
            e = " and ";
        } else {
            e = " ";
        }

        this.anoMes = "Time: " + yStr + y + e + mStr + m + " (" + (this.matriz.getAnoInicial() + this.ano) + ") ";
        this.anoMes = "Time: " + yStr + y + " (" + (this.matriz.getAnoInicial() + this.ano) + ") ";

        return this.anoMes;

    }

    /**
     *
     * @return
     */
    public String getHome() {
        return home;
    }

    /**
     *
     * @return
     */
    public String getDiretorio() {
        return diretorio;
    }

    private String getDateTime() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
        Date date = new Date();
        return dateFormat.format(date);
    }

    /**
     *
     * @param frame
     * @return
     * @throws InterruptedException
     */
    public boolean controle(TelaExecucao frame) throws IOException{
        //int x = this.getMatriz().getX();
        //int y = this.getMatriz().getY();  

        boolean flagPontoChegada = false;

        while (checkStatusExec() && !flagPontoChegada) {

            if (this.getExecucao() > this.getLimiteExecucao()) {
                return false;
            }

            this.calculaAnoMes(this.getnIteracao());
            //if(this.getAno() == 14 && flag_arquivo2030 == 0){
            //    this.getMatriz().gravaMatrizesResultado(this.getDiretorioResultados(), this.getNomeExecucao() + "_Exec " + this.getExecucao() + "_2030");
            //    flag_arquivo2030 = 1;
                
            //}
            //else if (this.getAno() == 34 && flag_arquivo2050 == 0){
            //    this.getMatriz().gravaMatrizesResultado(this.getDiretorioResultados(), this.getNomeExecucao() + "_Exec " + this.getExecucao() + "_2050");
            //    flag_arquivo2050 = 1;
            //}
            dispersao();

            this.addnIteracao();

            if ((this.getnIteracao() * this.getMatriz().getEscala()) % this.getMatriz().getIteracaoPorAno() == 0) {
                frame.getImagePanel().repaint();
                frame.setTitle("Execução: " + this.getExecucao() + " - " + this.getAnoMes());
            }

            if (this.matriz.isFinalPointDefined()) {
                flagPontoChegada = Matriz.verificaPontoChegada(this.A, this.matriz.getX(), this.matriz.getY(), areaChegada);
                if (flagPontoChegada || !checkStatusExec()) {
                    addTempoTotal(this.getAno());
                }
            }

        }

        this.setMedia(((this.getMedia() + this.getnIteracao()) / 2));

        return true;
    }

    public boolean checkStatusExec() {
        return (this.getAno() < this.getLimiteAno());
    }

    /**
     *
     */
    public void dispersao() {
        int l;
        int c;
        int v, v1, v2, v3, v4, v5, v6, v7, v8;
        double valueP1 = 0, valueP11 = 0, valueP111 = 0;
        Integer altura;
        boolean montante;
        boolean jusante;
        Random r = new Random();
        double pm, pn, fxn, fxm, distanciaRelativa;
        int zn, zm;
        int dm, dn;
        int ldir, cdir, dl, dc;
        int n, m;

        n = getMatriz().getN();
        m = getMatriz().getM();

        for (l = 1; l < n; l++) {
            for (c = 1; c < m; c++) {

                if (this.tipo == 0) {
                    switch (getAno()) {
                        case 3:
                            if (!"1".equals(getA()[826][520])) {
                                if (!"1".equals(getMA()[826][520])) {
                                    getMA()[826][520] = "1";
                                    this.MS[826][520] += 1;                                    
                                    if (MI[826][520] == 0) {
                                        MI[826][520] = getAno();
                                    } else {
                                        MI[826][520] = (MI[826][520] + getAno()) / 2;
                                    }
                                }
                            }
                            break;
                        case 10:
                            if (!"1".equals(getA()[954][441])) {
                                if (!"1".equals(getMA()[954][441])) {
                                    getMA()[954][441] = "1";
                                    MS[954][441] += 1;
                                    if (MI[954][441] == 0) {
                                        MI[954][441] = getAno();
                                    } else {
                                        MI[954][441] = (MI[954][441] + getAno()) / 2;
                                    }
                                }
                            }
                            break;
                        case 17:
                            if (!"1".equals(getA()[372][929])) {
                                if (!"1".equals(getMA()[372][929])) {
                                    getMA()[372][929] = "1";
                                    MS[372][929] += 1;
                                    if (MI[372][929] == 0) {
                                        MI[372][929] = getAno();
                                    } else {
                                        MI[372][929] = (MI[372][929] + getAno()) / 2;
                                    }
                                }
                            }
                            if (!"1".equals(getA()[396][892])) {
                                if (!"1".equals(getMA()[396][892])) {
                                    getMA()[396][892] = "1";
                                    MS[396][892] += 1;
                                    if (MI[396][892] == 0) {
                                        MI[396][892] = getAno();
                                    } else {
                                        MI[396][892] = (MI[396][892] + getAno()) / 2;
                                    }
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }

                if ("3".equals(getA()[l][c]) || "0".equals(getA()[l][c]) || "5".equals(getA()[l][c])) {

                    altura = Integer.parseInt(getMatriz().getMJM()[l][c]);
                    if ("1".equals(getA()[l - 1][c - 1])) {
                        v1 = 1;
                        if (Integer.parseInt(getMatriz().getMJM()[l - 1][c - 1]) > altura && Integer.parseInt(getMatriz().getMJM()[l - 1][c - 1]) >= 0) {
                            altura = Integer.parseInt(getMatriz().getMJM()[l - 1][c - 1]);
                        }
                    } else {
                        v1 = 0;
                    }
                    if ("1".equals(getA()[l - 1][c])) {
                        v2 = 1;
                        if (Integer.parseInt(getMatriz().getMJM()[l - 1][c]) > altura && Integer.parseInt(getMatriz().getMJM()[l - 1][c]) >= 0) {
                            altura = Integer.parseInt(getMatriz().getMJM()[l - 1][c]);
                        }
                    } else {
                        v2 = 0;
                    }
                    if ("1".equals(getA()[l - 1][c + 1])) {
                        v3 = 1;
                        if (Integer.parseInt(getMatriz().getMJM()[l - 1][c + 1]) > altura && Integer.parseInt(getMatriz().getMJM()[l - 1][c + 1]) >= 0) {
                            altura = Integer.parseInt(getMatriz().getMJM()[l - 1][c + 1]);
                        }
                    } else {
                        v3 = 0;
                    }
                    if ("1".equals(getA()[l][c - 1])) {
                        v4 = 1;
                        if (Integer.parseInt(getMatriz().getMJM()[l][c - 1]) > altura && Integer.parseInt(getMatriz().getMJM()[l][c - 1]) >= 0) {
                            altura = Integer.parseInt(getMatriz().getMJM()[l][c - 1]);
                        }
                    } else {
                        v4 = 0;
                    }
                    if ("1".equals(getA()[l][c + 1])) {
                        v5 = 1;
                        if (Integer.parseInt(getMatriz().getMJM()[l][c + 1]) > altura && Integer.parseInt(getMatriz().getMJM()[l][c + 1]) >= 0) {
                            altura = Integer.parseInt(getMatriz().getMJM()[l][c + 1]);
                        }
                    } else {
                        v5 = 0;
                    }
                    if ("1".equals(getA()[l + 1][c - 1])) {
                        v6 = 1;
                        if (Integer.parseInt(getMatriz().getMJM()[l + 1][c - 1]) > altura && Integer.parseInt(getMatriz().getMJM()[l + 1][c - 1]) >= 0) {
                            altura = Integer.parseInt(getMatriz().getMJM()[l + 1][c - 1]);
                        }
                    } else {
                        v6 = 0;
                    }
                    if ("1".equals(getA()[l + 1][c])) {
                        v7 = 1;
                        if (Integer.parseInt(getMatriz().getMJM()[l + 1][c]) > altura && Integer.parseInt(getMatriz().getMJM()[l + 1][c]) >= 0) {
                            altura = Integer.parseInt(getMatriz().getMJM()[l + 1][c]);
                        }
                    } else {
                        v7 = 0;
                    }
                    if ("1".equals(getA()[l + 1][c + 1])) {
                        v8 = 1;
                        if (Integer.parseInt(getMatriz().getMJM()[l + 1][c + 1]) > altura && Integer.parseInt(getMatriz().getMJM()[l + 1][c + 1]) >= 0) {
                            altura = Integer.parseInt(getMatriz().getMJM()[l + 1][c + 1]);
                        }
                    } else {
                        v8 = 0;
                    }

                    if (altura == Integer.parseInt(getMatriz().getMJM()[l][c])) {
                        montante = true;
                        jusante = false;
                    } else {
                        montante = false;
                        jusante = true;
                    }

                    if (montante) {
                        valueP1 = getMatriz().getDispersaoHidroviaMontante();
                        valueP11 = getMatriz().getDispersaoRioMontante();
                        valueP111 = getMatriz().getDispersaoAfluenteMontante();
                    } else if (jusante) {
                        valueP1 = getMatriz().getDispersaoHidroviaJusante();
                        valueP11 = getMatriz().getDispersaoRioJusante();
                        valueP111 = getMatriz().getDispersaoAfluenteJusante();
                    }

                    v = v1 + v2 + v3 + v4 + v5 + v6 + v7 + v8;

                    if (Integer.parseInt(getMatriz().getMD()[l][c]) <= 1) {
                        valueP1 = valueP1 * 9;
                        valueP11 = valueP11 * 9;
                        valueP111 = valueP111 * 9;
                    } else if (Integer.parseInt(getMatriz().getMD()[l][c]) > 1 && Integer.parseInt(getMatriz().getMD()[l][c]) <= 2) {
                        valueP1 = valueP1 * 7;
                        valueP11 = valueP11 * 7;
                        valueP111 = valueP111 * 7;
                    } else if (Integer.parseInt(getMatriz().getMD()[l][c]) > 2 && Integer.parseInt(getMatriz().getMD()[l][c]) <= 3) {
                        valueP1 = valueP1 * 4;
                        valueP11 = valueP11 * 4;
                        valueP111 = valueP111 * 4;
                    } else if (Integer.parseInt(getMatriz().getMD()[l][c]) > 3 && Integer.parseInt(getMatriz().getMD()[l][c]) <= 4) {
                        valueP1 = valueP1 * 1;
                        valueP11 = valueP11 * 1;
                        valueP111 = valueP111 * 1;
                    } else if (Integer.parseInt(getMatriz().getMD()[l][c]) > 4 && Integer.parseInt(getMatriz().getMD()[l][c]) <= 6) {
                        valueP1 = valueP1 * 0.8;
                        valueP11 = valueP11 * 0.8;
                        valueP111 = valueP111 * 0.8;
                    } else if (Integer.parseInt(getMatriz().getMD()[l][c]) > 6 && Integer.parseInt(getMatriz().getMD()[l][c]) <= 11) {
                        valueP1 = valueP1 * 0.5;
                        valueP11 = valueP11 * 0.5;
                        valueP111 = valueP111 * 0.5;
                    } else if (Integer.parseInt(getMatriz().getMD()[l][c]) > 11 && Integer.parseInt(getMatriz().getMD()[l][c]) <= 30) {
                        valueP1 = valueP1 * 0.2;
                        valueP11 = valueP11 * 0.2;
                        valueP111 = valueP111 * 0.2;
                    }

                    if (null != getA()[l][c]) {
                        switch (getA()[l][c]) {
                            case "0":
                                if (r.nextDouble() * v >= valueP11) {
                                    if (!"1".equals(getMA()[l][c])) {
                                        getMA()[l][c] = "1";
                                        MS[l][c] += 1;
                                        if (getAno() < 15){
                                            MS2030[l][c] += 1;
                                        }
                                        if (MI[l][c] == 0) {
                                            MI[l][c] = getAno();
                                        } else {
                                            MI[l][c] = (MI[l][c] + getAno()) / 2;
                                        }
                                    }
                                }
                                break;
                            case "3":
                                if (r.nextDouble() * v >= valueP111) {
                                    if (!"1".equals(getMA()[l][c])) {
                                        getMA()[l][c] = "1";
                                        MS[l][c] += 1;
                                        if (getAno() < 15){
                                            MS2030[l][c] += 1;
                                        }
                                        if (MI[l][c] == 0) {
                                            MI[l][c] = getAno();
                                        } else {
                                            MI[l][c] = (MI[l][c] + getAno()) / 2;
                                        }
                                    }
                                }
                                break;
                            default:
                                if (r.nextDouble() * v >= valueP1) {
                                    if (!"1".equals(getMA()[l][c])) {
                                        getMA()[l][c] = "1";
                                        MS[l][c] += 1;
                                        if (getAno() < 15){
                                            MS2030[l][c] += 1;
                                        }
                                        if (MI[l][c] == 0) {
                                            MI[l][c] = getAno();
                                        } else {
                                            MI[l][c] = (MI[l][c] + getAno()) / 2;
                                        }
                                    }
                                }
                                break;
                        }
                    }

                    if ("1".equals(getMA()[l][c])) {
                        if (r.nextDouble() >= getMatriz().getQuantidadePulo()) {
                            if (r.nextDouble() < 0.98) {
                                distanciaRelativa = (getMatriz().getDistanciaPulo() / 100.00);
                                zn = (int) (((getMatriz().getN() - 1) - l) * distanciaRelativa);
                                zm = (int) (((getMatriz().getM() - 1) - c) * distanciaRelativa);
                            } else {
                                distanciaRelativa = (getMatriz().getDistanciaPuloGrande() / 100.00);
                                zn = (int) (((getMatriz().getN() - 1) - l) * distanciaRelativa);
                                zm = (int) (((getMatriz().getM() - 1) - c) * distanciaRelativa);
                            }
                            if (zn < 1) {
                                zn = 1;
                            }
                            if (zm < 1) {
                                zm = 1;
                            }
                            dm = r.nextInt(zm);
                            dn = r.nextInt(zn);
                            pm = (dm * 100.00 / zm) / 100.00;
                            pn = (dn * 100.00 / zn) / 100.00;

                            fxm = Math.exp(-1 / (2 * 0.16) * Math.pow(pm, 2));
                            fxn = Math.exp(-1 / (2 * 0.16) * Math.pow(pn, 2));

                            if (r.nextDouble() < fxn) {
                                if (r.nextDouble() > r.nextDouble()) {
                                    ldir = 1;
                                } else {
                                    ldir = -1;
                                }

                                dl = dn * ldir + l;

                                if (dl < 1) {
                                    dl = 1;
                                }
                                if (dl > getMatriz().getN() - 1) {
                                    dl = (getMatriz().getN() - 1);
                                }
                            } else {
                                dl = l;
                            }
                            if (r.nextDouble() < fxm) {
                                if (r.nextDouble() > r.nextDouble()) {
                                    cdir = 1;
                                } else {
                                    cdir = -1;
                                }

                                dc = dm * cdir + c;

                                if (dc < 1) {
                                    dc = 1;
                                }
                                if (dc > getMatriz().getM() - 1) {
                                    dc = (getMatriz().getM() - 1);
                                }
                            } else {
                                dc = c;
                            }

                            if ("0".equals(getA()[dl][dc]) || "5".equals(getA()[dl][dc]) || "3".equals(getA()[dl][dc])) {
                                if (!"1".equals(getMA()[dl][dc])) {
                                    getMA()[dl][dc] = "1";
                                    MS[dl][dc] += 1;
                                    if (getAno() < 15){
                                        MS2030[dl][dc] += 1;
                                    }
                                    if (MI[dl][dc] == 0) {
                                        MI[dl][dc] = getAno();
                                    } else {
                                        MI[dl][dc] = (MI[dl][dc] + getAno()) / 2;
                                    }
                                }
                            }

                        }
                    }
                }

            }
        }
        copyMAtoA();

    }

    public String getDiretorioResultados() {
        return diretorioResultados;
    }
    
    
}
