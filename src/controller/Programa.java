/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import gui.TelaInicial;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.DefaultTableModel;
import model.Diretorio;
import model.Matriz;
import model.Resultado;
import util.SqlLiteConexao;

/**
 *
 * @author CBEIH
 */
public final class Programa {

    private SqlLiteConexao conec;
    private boolean conexaoSQL;
    private int statusDB; // 0 - nao existe db / 1 - db online / 2 - db offline
    private boolean conexaoInternet;
    private Diretorio dir;
    private ArrayList<Matriz> listaMatrizes;
    private ArrayList<Matriz> listaMatrizesUser;
    private static int nExec;
    private int paradaX, paradaY;
    private List<Resultado> listResultado;
    private TelaInicial telaInicial;

    /**
     *
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws IOException
     */
    public Programa() throws SQLException, ClassNotFoundException, IOException {
        criarDiretorioGLfUser();
        verificaConexaoInternet();
        setConec();
    }

    /**
     *
     * @return
     */
    public boolean isConexaoInternet() {
        return conexaoInternet;
    }

    /**
     *
     * @param conexaoInternet
     */
    public void setConexaoInternet(boolean conexaoInternet) {
        this.conexaoInternet = conexaoInternet;
    }

    /**
     *
     * @return
     */
    public boolean isConexaoSQL() {
        return conexaoSQL;
    }

    /**
     *
     * @param conexaoSQL
     */
    public void setConexaoSQL(boolean conexaoSQL) {
        this.conexaoSQL = conexaoSQL;
    }

    /**
     *
     * @return
     */
    public SqlLiteConexao getConec() {
        return conec;
    }

    /**
     *
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws IOException
     */
    public void setConec() throws SQLException, ClassNotFoundException, IOException {
        try {
            conec = new SqlLiteConexao(this);
            conexaoSQL = true;
        } catch (SQLException | ClassNotFoundException | IOException e) {
            conexaoSQL = false;
        }

    }

    /**
     *
     * @return
     */
    public Diretorio getDir() {
        return dir;
    }

    /**
     *
     * @param dir
     */
    public void setDir(Diretorio dir) {
        this.dir = dir;
    }

    public TelaInicial getTelaInicial() {
        return telaInicial;
    }

    public void setTelaInicial(TelaInicial telaInicial) {
        this.telaInicial = telaInicial;
    }

    public void carregaResultado(DefaultTableModel modeloTable, int quantidade) {
        this.listResultado = this.dir.carregarResultados(0);
        for (Resultado r : this.listResultado) {
            modeloTable.addRow(new Object[]{r.getNome(), r.getData()});
        }
    }

    public List<Resultado> getListResultado() {
        return listResultado;
    }

    public void setListResultado(List<Resultado> listResultado) {
        this.listResultado = listResultado;
    }

    /**
     *
     */
    public void verificaConexaoInternet() {
        try {
            java.net.URL mandarMail = new java.net.URL("http://www.cbeih.org/");
            java.net.URLConnection conn = mandarMail.openConnection();
            conn.setUseCaches(false);
            java.net.HttpURLConnection httpConn = (java.net.HttpURLConnection) conn;
            httpConn.connect();
            int x = httpConn.getResponseCode();
            if (x == 200) {
                conexaoInternet = true;
            }
        } catch (java.net.MalformedURLException urlmal) {
            conexaoInternet = false;
        } catch (java.io.IOException ioexcp) {
            conexaoInternet = false;
        }
    }

    /**
     *
     */
    public void criarDiretorioGLfUser() {
        dir = new Diretorio();
    }

    /**
     *
     * @return
     */
    public int getStatusDB() {
        return statusDB;
    }

    /**
     *
     * @param statusDB
     */
    public void setStatusDB(int statusDB) {
        this.statusDB = statusDB;
    }

    /**
     *
     * @return
     */
    public ArrayList<Matriz> getListaMatrizes() {
        return listaMatrizes;
    }

    public ArrayList<Matriz> getListaMatrizesUser() {
        return listaMatrizesUser;
    }

    /**
     *
     * @param listaMatrizes
     */
    public void setListaMatrizes(ArrayList<Matriz> listaMatrizes) {
        this.listaMatrizes = listaMatrizes;
    }

    public void setListaMatrizesUser(ArrayList<Matriz> listaMatrizesUser) {
        this.listaMatrizesUser = listaMatrizesUser;
    }

    /**
     *
     */
    public static void addNExec() {
        Programa.nExec = Programa.nExec + 1;
    }

    /**
     *
     */
    public static void subNExec() {
        Programa.nExec = Programa.nExec - 1;
    }

    /**
     *
     * @return
     */
    public static boolean isNExecFull() {
        return (Programa.nExec == 2);
    }

    /**
     *
     * @return
     */
    public int getnExec() {
        return nExec;
    }

    /**
     *
     * @param nExec
     */
    public void setnExec(int nExec) {
        Programa.nExec = nExec;
    }

    public int getParadaX() {
        return paradaX;
    }

    public void setParadaX(int paradaX) {
        this.paradaX = paradaX;
    }

    public int getParadaY() {
        return paradaY;
    }

    public void setParadaY(int paradaY) {
        this.paradaY = paradaY;
    }

    @Override
    public String toString() {
        return "Programa{" + "conec=" + conec + ", conexaoSQL=" + conexaoSQL + ", statusDB=" + statusDB + ", conexaoInternet=" + conexaoInternet + ", dir=" + dir + '}';
    }

}
