/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import gui.TelaInicial;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;

/**
 *
 * @author CBEIH
 */
public class ProjectMain {

    /**
     *
     * @param args
     * @throws FileNotFoundException
     * @throws IOException
     * @throws InterruptedException
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public static void main(String[] args) throws FileNotFoundException, IOException, InterruptedException, SQLException, ClassNotFoundException {

        Programa programa = new Programa();

        programa.setTelaInicial(new TelaInicial(programa));
        programa.getTelaInicial().setVisible(true);
    }
}
