/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import gui.TelaExecucao;
import gui.TelaInicial;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author CBEIH
 */
public class ExecControllerThread extends Thread {

    private final int indice;
    private final Execucao execucao;
    private final TelaExecucao frame;
    private final Programa programa;

    /**
     *
     * @param execucao
     * @param indice
     * @param programa
     * @param frame
     */
    public ExecControllerThread(Execucao execucao, int indice, Programa programa, TelaExecucao frame) {
        super("Controle de Execucao do Algortimo");
        this.execucao = execucao;
        this.indice = indice;
        this.frame = frame;
        this.programa = programa;

    }

    @Override
    public void run() {
        boolean continuar = true;
        synchronized (execucao.getMatriz()) {
            while (continuar && !this.isInterrupted()) {

                execucao.limpaExecucao();
                if (execucao.getTipo() == 0) {
                    execucao.setMatriz(programa.getListaMatrizes().get(this.indice));
                } else {
                    execucao.setMatriz(programa.getListaMatrizesUser().get(this.indice));
                }
                execucao.initAandMA();
                execucao.copyMBtoA();
                execucao.copyMBtoMA();
                execucao.addExecucao();
                frame.getImagePanel().setTempoChegadaTotal(execucao.getTempoTotal());

                try {
                    continuar = execucao.controle(frame);

                } catch (IOException ex) {
                    Logger.getLogger(ExecControllerThread.class.getName()).log(Level.SEVERE, null, ex);
                }

            }

        }
    }

}
