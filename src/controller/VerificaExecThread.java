/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import gui.TelaExecucao;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import gui.ImagePanel;

/**
 *
 * @author CBEIH
 */
public class VerificaExecThread extends Thread {

    private Thread t;
    private TelaExecucao frame;
    private Execucao execucao;
    //private TelaInicial telaInicial;

    /**
     *
     */
    public VerificaExecThread() {
        super("Controle de Threads");
    }

    /**
     *
     * @param t
     * @param frame
     * @param execucao
     */
    public VerificaExecThread(Thread t, TelaExecucao frame, Execucao execucao/*,TelaInicial telaInicial*/) {
        super("Controle de Threads");
        this.t = t;
        this.frame = frame;
        this.execucao = execucao;
        //this.telaInicial = telaInicial;
    }

    @Override
    public void run() {
        while (t.isAlive()) {

        }
        try {
            if (execucao.getChegada() == 1) {
                frame.getImagePanel().setMatrizMediaAno(execucao.getMI());
            }
            execucao.getMatriz().gravaMatrizesResultado(execucao.getDiretorioResultados(), execucao.getNomeExecucao());
            ImagePanel.geraImagemResultado(frame, "png", new FileOutputStream(execucao.getDiretorio() + "/Resultados/" + execucao.getNomeExecucao() + ".png"));
            JOptionPane.showMessageDialog(frame, "O resultado foi salvo no caminho: "
                    + execucao.getDiretorio() + "/Resultados/" + execucao.getNomeExecucao() + ".png", "Resultado",
                    JOptionPane.INFORMATION_MESSAGE);
        } catch (IOException ex) {
            Logger.getLogger(VerificaExecThread.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(frame, ex, "ERRO",
                    JOptionPane.ERROR_MESSAGE);
        }

        try {
            this.execucao.getM().limpaMatriz();
            this.execucao.limpaExecucao();
        } catch (IOException ex) {
            Logger.getLogger(TelaExecucao.class.getName()).log(Level.SEVERE, null, ex);
        }

        //telaInicial.initTableResultados();
        frame.getTelaInicial().initTableResultados();

        frame.dispose();
    }

}
