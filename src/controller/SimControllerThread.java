/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import gui.TelaExecucao;
import java.awt.Cursor;
import javax.swing.JOptionPane;

/**
 *
 * @author CBEIH
 */
public class SimControllerThread extends Thread {

    int simulacao;
    String nomeSimulacao;
    int nExecucao;
    int tempoExecucao;
    String hidrografia;
    //TelaInicial frame;
    Programa programa;
    int tipo;

    /**
     *
     * @param frame
     * @param simulacao
     * @param nomeSimulacao
     * @param nExecucao
     * @param tempoExecucao
     * @param hidrografia
     * @param programa
     * @param tipo
     */
    public SimControllerThread(/*TelaInicial frame, */int simulacao, String nomeSimulacao, int nExecucao, int tempoExecucao, String hidrografia, Programa programa, int tipo) {
        super("Simulacao do Algortimo");
        //this.frame = frame;
        this.simulacao = simulacao;
        this.nomeSimulacao = nomeSimulacao;
        this.nExecucao = nExecucao;
        this.tempoExecucao = tempoExecucao;
        this.hidrografia = hidrografia;
        this.programa = programa;
        this.tipo = tipo;
    }

    /**
     *
     */
    public SimControllerThread() {

    }

    @Override
    public void run() {
        if (tipo == 0) {
            if (!programa.getListaMatrizes().get(simulacao).isPreProcessada()) {
                if (-1 != simulacao && -1 != nExecucao && -1 != tempoExecucao
                        && !"".equals(hidrografia)) {

                    programa.getTelaInicial().addControleExec();
                    programa.getTelaInicial().getBotaoIniciarSimulacao().setEnabled(false);
                    programa.getTelaInicial().getContentPane().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

                    Execucao execucao;

                    if (!"".equals(nomeSimulacao)) {
                        execucao = new Execucao(nomeSimulacao, programa.getDir());
                    } else {
                        execucao = new Execucao(programa.getDir());
                    }

                    try {

                        if (programa.getTelaInicial().getjButton7().isEnabled()) {
                            execucao.setChegada(1);
                            execucao.setAreaChegada(15);
                        } else {
                            execucao.setChegada(0);
                        }

                        programa.getListaMatrizes().get(simulacao).preProcessaMatriz(programa.getDir());
                        execucao.setMatriz(programa.getListaMatrizes().get(simulacao));
                        execucao.iniciaMatrizesResultado();
                        execucao.initAandMA();
                        execucao.copyMBtoA();
                        execucao.copyMBtoMA();
                        execucao.setLimiteExecucao(nExecucao);
                        execucao.setLimiteAno(tempoExecucao);
                        execucao.setTipo(0);

                    } catch (Exception ex) {
                        JOptionPane.showMessageDialog(programa.getTelaInicial(), ex, "ERRO",
                                JOptionPane.ERROR_MESSAGE);
                    }

                    try {
                        programa.getTelaInicial().getBotaoIniciarSimulacao().setEnabled(true);
                        programa.getTelaInicial().getContentPane().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                        TelaExecucao frameExec = new TelaExecucao(simulacao/*programa.getListaMatrizes().get(simulacao)*/, execucao, programa/*, programa.getTelaInicial()*/);
                    } catch (Exception ex) {
                        JOptionPane.showMessageDialog(programa.getTelaInicial(), ex, "ERRO",
                                JOptionPane.ERROR_MESSAGE);
                    }

                } else {
                    JOptionPane.showMessageDialog(programa.getTelaInicial(), "Favor preencher todos os campos obrigatórios.", "Preenchimento dos campos",
                            JOptionPane.WARNING_MESSAGE);
                }
                programa.getTelaInicial().subControleExec();
            } else {
                JOptionPane.showMessageDialog(programa.getTelaInicial(), "Simulação já sendo executada. Favor aguardar o término para executar uma nova.", "Simulação em Andamento",
                        JOptionPane.WARNING_MESSAGE);
            }
        } else if (!programa.getListaMatrizesUser().get(simulacao).isPreProcessada()) {
            if (-1 != simulacao && -1 != nExecucao && -1 != tempoExecucao
                    && !"".equals(hidrografia)) {

                programa.getTelaInicial().addControleExec();
                programa.getTelaInicial().getBotaoIniciarSimulacao().setEnabled(false);
                programa.getTelaInicial().getContentPane().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

                Execucao execucao;

                if (!"".equals(nomeSimulacao)) {
                    execucao = new Execucao(nomeSimulacao, programa.getDir());
                } else {
                    execucao = new Execucao(programa.getDir());
                }

                try {

                    if (programa.getTelaInicial().getjButton7().isEnabled()) {
                        execucao.setChegada(1);
                        execucao.setAreaChegada(15);
                    } else {
                        execucao.setChegada(0);
                    }

                    programa.getListaMatrizesUser().get(simulacao).preProcessaMatriz(programa.getDir());
                    execucao.setMatriz(programa.getListaMatrizesUser().get(simulacao));
                    execucao.iniciaMatrizesResultado();
                    execucao.initAandMA();
                    execucao.copyMBtoA();
                    execucao.copyMBtoMA();
                    execucao.setLimiteExecucao(nExecucao);
                    execucao.setLimiteAno(tempoExecucao);
                    execucao.setTipo(1);
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(programa.getTelaInicial(), ex, "ERRO",
                            JOptionPane.ERROR_MESSAGE);
                }

                try {
                    programa.getTelaInicial().getBotaoIniciarSimulacao().setEnabled(true);
                    programa.getTelaInicial().getContentPane().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                    TelaExecucao frameExec = new TelaExecucao(simulacao/*programa.getListaMatrizesUser().get(simulacao)*/, execucao, programa/*, programa.getTelaInicial()*/);
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(programa.getTelaInicial(), ex, "ERRO",
                            JOptionPane.ERROR_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(programa.getTelaInicial(), "Favor preencher todos os campos obrigatórios.", "Preenchimento dos campos",
                        JOptionPane.WARNING_MESSAGE);
            }
            programa.getTelaInicial().subControleExec();
        } else {
            JOptionPane.showMessageDialog(programa.getTelaInicial(), "Simulação já sendo executada. Favor aguardar o término para executar uma nova.", "Simulação em Andamento",
                    JOptionPane.WARNING_MESSAGE);
        }
    }

}
