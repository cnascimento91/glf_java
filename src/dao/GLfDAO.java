/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.Matriz;
import util.SqlLiteConexao;

/**
 *
 * @author CBEIH
 */
public class GLfDAO {

    private final SqlLiteConexao conexao;

    /**
     *
     * @param conexao
     */
    public GLfDAO(SqlLiteConexao conexao) {
        this.conexao = conexao;
    }

    /*INSERT INTO `glf` (`SourceHidrografia`, `sourceAltitude`, `sourceDensidade`, `id`, `URLHidrografia`, 
    `URLAltitude`, `URLDensidade`, `anoInicial`, `coordenadaFinalX`, `coordenadaFinalY`, `escala`, `cellSize`, 
    `pixelSize`, `dispersaoHidroviaMontante`, `dispersaoHidroviaJusante`, `dispersaoRioMontante`, `dispersaoRioJusante`, 
    `dispersaoAfluenteMontante`, `dispersaoAfluenteJusante`, `distanciaPulo`, `distanciaPuloGrande`, `quantidadePulo`, `iteracaoPorAno`) VALUES
        ('brasil_4km_NEW.csv', 'brasil_4km_altitude_NEW.csv', 'brasil_4km_2015_densidade.csv', 0, 'http://www.cbeih.org/GLf/Files/brasil_4km_NEW.csv', 
        'http://www.cbeih.org/GLf/Files/brasil_4km_altitude_NEW.csv', 'http://www.cbeih.org/GLf/Files/brasil_4km_2015_densidade.csv', 
        2016, 273, 47, 25, 1, 1, 0.125, 0.125, 0.95, 0.25, 0.99, 0.5, 15, 100, 0.4, 400),
        
                ('brasil_4km_98_NEW.csv', 'brasil_4km_altitude_NEW.csv', 'brasil_4km_2015_densidade.csv', 
        1, 'http://www.cbeih.org/GLf/Files/brasil_4km_98_NEW.csv', 'http://www.cbeih.org/GLf/Files/brasil_4km_altitude_NEW.csv', 
        'http://www.cbeih.org/GLf/Files/brasil_4km_2015_densidade.csv', 1998, 273, 47, 25, 1, 1, 0.125, 0.125, 0.95, 0.25, 0.99, 
        0.5, 15, 100, 0.4, 400);*/
    /**
     *
     * @param m
     * @param id
     * @return
     * @throws SQLException
     */
    public boolean insereGLF(Matriz m, int id) throws SQLException {
        StringBuilder sql = new StringBuilder();
        sql.append("INSERT INTO glf VALUES ('");
        sql.append(m.getNome());
        sql.append("','");
        sql.append(m.getMatrizRioCSV());
        sql.append("','");
        sql.append(m.getMatrizJusanteMontanteCSV());
        sql.append("','");
        sql.append(m.getMatrizDensidadeCSV());
        sql.append("',");
        sql.append(id);
        sql.append(",'");
        sql.append(m.getURLmatrizRioCSV());
        sql.append("','");
        sql.append(m.getURLmatrizJusanteMontanteCSV());
        sql.append("','");
        sql.append(m.getURLmatrizDensidadeCSV());
        sql.append("',");
        sql.append(m.getAnoInicial());
        sql.append(",");
        sql.append(m.getX());
        sql.append(",");
        sql.append(m.getY());
        sql.append(",");
        sql.append(m.getEscala());
        sql.append(",");
        sql.append(m.getCellSize());
        sql.append(",");
        sql.append(m.getPixelSize());
        sql.append(",");
        sql.append(m.getDispersaoHidroviaMontante());
        sql.append(",");
        sql.append(m.getDispersaoHidroviaJusante());
        sql.append(",");
        sql.append(m.getDispersaoRioMontante());
        sql.append(",");
        sql.append(m.getDispersaoRioJusante());
        sql.append(",");
        sql.append(m.getDispersaoAfluenteMontante());
        sql.append(",");
        sql.append(m.getDispersaoAfluenteJusante());
        sql.append(",");
        sql.append(m.getDistanciaPulo());
        sql.append(",");
        sql.append(m.getDistanciaPuloGrande());
        sql.append(",");
        sql.append(m.getQuantidadePulo());
        sql.append(",");
        sql.append(m.getIteracaoPorAno());
        sql.append(")");

        return conexao.executeSQL(sql.toString()) > 0;
    }

    /**
     *
     * @return @throws SQLException
     * @throws java.io.IOException
     */
    public ArrayList<Matriz> selectAll() throws SQLException, IOException {
        ArrayList<Matriz> matrizes = new ArrayList<>();
        String sql = "SELECT * FROM glf";
        ResultSet rs = conexao.executeQuerySQL(sql);
        while (rs.next()) {
            matrizes.add(convertResultSet(rs));
        }
        return matrizes;
    }

    /**
     *
     * @param id
     * @return
     * @throws SQLException
     * @throws java.io.IOException
     */
    public Matriz findByID(int id) throws SQLException, IOException {
        Matriz matriz = null;
        String sql = "SELECT * FROM glf WHERE id = " + id;
        ResultSet rs = conexao.executeQuerySQL(sql);

        if (rs.next()) {
            matriz = convertResultSet(rs);
        }

        return matriz;
    }

    /*(`id`, ``, 
    ``, ``, ``, ``, ``, ``, ``, 
    ``, ``, ``, ``, ``, 
    ``, ``, ``, ``, ``, ``)*/
    private Matriz convertResultSet(ResultSet rs) throws SQLException, IOException {
        Matriz m = new Matriz(rs.getString("SourceHidrografia"), rs.getString("sourceAltitude"), rs.getString("sourceDensidade"),
                rs.getInt("coordenadaFinalX"), rs.getInt("coordenadaFinalY"), rs.getInt("escala"), rs.getInt("anoInicial"),
                rs.getDouble("cellSize"), rs.getInt("pixelSize"),
                rs.getDouble("dispersaoHidroviaMontante"), rs.getDouble("dispersaoHidroviaJusante"), rs.getDouble("dispersaoRioMontante"),
                rs.getDouble("dispersaoRioJusante"), rs.getDouble("dispersaoAfluenteMontante"), rs.getDouble("dispersaoAfluenteJusante"),
                rs.getInt("distanciaPulo"), rs.getInt("distanciaPuloGrande"), rs.getInt("quantidadePulo"), rs.getInt("iteracaoPorAno"),
                rs.getString("URLHidrografia"), rs.getString("URLAltitude"), rs.getString("URLDensidade"), rs.getString("Nome"));
        return m;
    }

    /**
     *
     * @return @throws SQLException
     */
    /*public boolean atualizaMatriz(Matriz matriz) throws SQLException {
        String sql = "UPDATE cliente SET nome = '" + cliente.getNome();
        sql += "', cpf = '" + cliente.getCpf();
        sql += "', dataNascimento = '" + cliente.getDataNascimento();
        sql += "', identidade = '" + cliente.getIdentidade();
        sql += "', endereco = '" + cliente.getEndereco();
        sql += "', email = '" + cliente.getEmail();
        sql += "', telefone = '" + cliente.getTelefone();
        sql += "' WHERE numeroCliente = " + cliente.getNumeroCliente();

        return conexao.executeSQL(sql) > 0;
    }*/
}
